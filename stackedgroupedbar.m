function [h] = stackedgroupedbar(stackData)
NumGroupsPerAxis = size(stackData, 1);
NumStacksPerGroup = size(stackData, 2);
groupBins = 1:NumGroupsPerAxis;
MaxGroupWidth = 0.65; % Fraction of 1. If 1, then we have all bars in groups touching
groupOffset = MaxGroupWidth/NumStacksPerGroup;
figure
    hold on; 
    xtick = [];   
for i=1:NumStacksPerGroup
    Y = squeeze(stackData(:,i,:));
    internalPosCount = i - ((NumStacksPerGroup+1) / 2);
    groupDrawPos = (internalPosCount)* groupOffset + groupBins;   
    h(i,:) = bar(Y, 'stacked');
    set(h(i,:),'BarWidth',groupOffset);
    set(h(i,:),'XData',groupDrawPos);
    xtick = [xtick; groupDrawPos(:)];
end
hold off;
set(gca,'XTickMode','manual');
xts = sort(xtick);
%set(gca,'XTickLabelMode','manual','XTick',xts,'XTickLabel',groupLabels);
end 

