function [x,y]=subplot_arranger(d)

% thing for arranging arbitrary number of subplots. input d, the number of
% sub-plots to generate

targ_ratio=16/9; % set this ratio as x/y, the *target* ratio. E.g. 3/2 would be 3 cols, 2 rows. 16/9 would be 16 columns, 9 rows (obv. this is a target, not the actual number of rows/cols produced)

x=ceil(sqrt(d/targ_ratio));
y=ceil(d/x);

% uncomment the following to generate some sample plots to see how it looks
% at different ratios

% for ii=1:d
% subplot(x,y,ii)
% plot(rand(20,1))
% end