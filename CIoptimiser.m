function out=CIoptimiser(X,Sd,objective,constr,CIeq,CIeqform)

% this is a self-contained function which optimises composite indicator
% weights using nonlinear regression (penalised splines).
%
% Requires an input matrix X with columns representing k variables and rows representing N
% samples.
%
% Sd is a k-length vector of desired influences (defaults to equal
% influence).
%
% objective is set to 1 for "Adjust" and 2 for "Maximize"
%
% constr is set to 0 for no constraints on weights, 1 for weights sum to
% one, and 2 for weights sum to one and are constrained to be positive.
%
% CIeq is the aggregation form of the CI. Set to 'ArAv' for arithmetic average,
% 'GeAv' for geometric average, or 'Cust' to use custom aggregation
% function.
%
% CIeqform is a function handle specified only if CIeq='Cust'
%
% Uses fminsearch as optimiser. If this doesn't work, may need to look at
% other solvers.
%
% Output is a structure which has:
%     wopt = optimised weights
%     Sopt = Si at optimised weights
%     plus a few other things that you can figure out from looking at the bottom of this function.
    
usebasicopt=0; % set this to 0 to use Matlab's inbuilt optimiser (rather than toolbox optimiser). Results in unconstrained optimisation.
MaxFE=1000; % this is the maximum number of function evaluations in the optimisation
[n,k]=size(X);

%% Set defaults

if nargin<2
    Sd=ones(k,1)/k; % default equal target influence
else
    Sd=Sd/sum(Sd(:));
end

if nargin<3
    constr=0;
end

if nargin<4
    CIeq= @(Xw) mean(X,2); % defualt unweighted arithmetic av.
elseif strcmp(CIeq,'ArAv')==1
    obj_agg=1;
    CIeq= @(Xw) sum(Xw,2); % weighted arithmetic av. - sum the weighted variables Xw
elseif strcmp(CIeq,'GeAv')==1
    obj_agg=2;
    CIeq= @(Xw) prod(Xw,2); % weighted geometric av. - multiply the weighted variables Xw
elseif strcmp(CIeq,'HarAv')==1
    obj_agg=3;
    CIeq= @(Xw) 1./sum(Xw,2); % weighted harmonic av. - divide 1 by the sum of the weighted variables Xw
elseif strcmp(CIeq,'Cust')==1
    obj_agg=4;
    CIeq=CIeqform; % custom function
end

%% Check toolboxes
verinfo=ver;
OptTB=any(strcmp('Optimization Toolbox', {verinfo.Name})); % flag for stats toolbox
if OptTB==0 
    warning('It looks like you dont have the optimisation toolbox. Defaulting to unconstrained optimisation of weights.')
end
%% Define objective function

    function [obj,S]=CIoptint(w) % this is the objective function which we want to minimise
        if obj_agg==1
            Xw=X.*repmat(w(:)',n,1); % For ArAv, multiply X by weights
        elseif obj_agg==2
            Xw=X.^repmat(w(:)',n,1); % For GeAv, X with weigths as exponentials
        elseif obj_agg==3
            Xw=repmat(w(:)',n,1)./X; % For HarAv, divide weights by X
        elseif obj_agg==4
            %Xw=...Custom equation, here you can add any aggregation
        end
        yw=CIeq(Xw); % now use aggregation method as specified by CIeq
        S=spline_SA(yw,X); % get Si using splines
        if objective==1
            Snorm=S/sum(S(:)); % normalise Si so they sum to 1
            obj= sum((Sd(:)-Snorm(:)).^2); % find sum of square differences between desired and actual
        elseif objective==2
            obj= sum(Sd(:)-S(:)); % maximize av. Si
        end
    end

%% Run optimisation

if OptTB==0 || usebasicopt==1
    [wopt,objfinal,exitflag,optoutput] = fminsearch(@CIoptint,Sd,optimset('MaxFunEvals',MaxFE,'Display','iter'));
    wopt=wopt/sum(wopt); % standardise to sum to 1
else
    if constr==0 % unconstrained weights
        [wopt,objfinal,exitflag,optoutput] = fminsearch(@CIoptint,Sd,optimoptions('fminsearch','MaxFunctionEvaluations',MaxFE,'Display','iter'));
    elseif constr==1 % weights constrained to sum to 1
        [wopt,objfinal,exitflag,optoutput] = fmincon(@CIoptint,Sd,[],[],ones(1,k),1,[],[],[],optimoptions('fmincon','MaxFunctionEvaluations',MaxFE,'Display','iter'));
    elseif constr==2 % weights constrained to be positive and to sum to 1
        solv_choice=menu('Select solver','Local','Global (Multistart)');
        if solv_choice==1
            [wopt,objfinal,exitflag,optoutput] = fmincon(@CIoptint,Sd,[],[],ones(1,k),1,zeros(k,1),[],[],optimoptions('fmincon','MaxFunctionEvaluations',MaxFE,'Display','iter'));
        elseif solv_choice==2
            ms = MultiStart;
            opts=optimoptions('fmincon','MaxFunctionEvaluations',MaxFE,'Display','iter');
            mums=createOptimProblem('fmincon','x0',Sd,'objective',@CIoptint,'Aeq',ones(1,k),'beq',1,'lb',zeros(k,1),'options',opts);
            pts=randmatrix(X);
            tpoints = CustomStartPointSet(pts);
            show_pts =list(tpoints); % Assumes tpoints is a CustomStartPointSet
            fprintf('CustomStartPointSet>> .\n')
            disp(show_pts) % display the set of starting points
            [wopt,objfinal,exitflag,optoutput]=run(ms,mums,tpoints); % run the multistart
        end
        
    end
end
%% Write outputs

[obj,Sopt]=CIoptint(wopt);

out.wopt=wopt;
out.Sopt=Sopt;
out.opt.constr=constr;
out.opt.obj=obj;
out.opt.objfinal=objfinal;
out.opt.exitflag=exitflag;
out.opt.optoutput=optoutput;

out.X=X;
out.CIeq=CIeq;
out.Sd=Sd;

end