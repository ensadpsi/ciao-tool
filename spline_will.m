function [yhat, par]=spline_will(x,y,plott,p,Kset,xx)

% A fully-automated smoothing spline fit for 1D data - automatic smoothing
% parameter selection and knot location selection. Fits p-order splines,
% where p is specified. Number of knots can be specified or calculated from
% a candidate set. Reduces to polynomial fit if 0 knots specified.
%
% Also includes
% "variability bands", in the sense of Ruppert p.135. Basically they are
% confidence intervals of E(y|x) as a result of the noise in the data, and
% include bias correction
%
% Also includes "prediction intervals", which, with 95% confidence,
% predict the region in which any new (and existing) data points fall. This is wider than
% the former region because it is inference on y, rather than E(y|x). See
% Ruppert p.136-7
%
% INPUTS
%       y - column vector of inputs
%       x - column vectot of outputs
%       plott - set to 1 if want plot, otherwise no plot generated (default
%       no plot), or set plott=2 for plot without confidence intervals (a
%       bit less computationally demanding)
%       p - order of spline (default 3)
%       xx - vector of points to make predictions at (defaults to 50 evenly spaced)
%       Kset - row vector of numbers of knots to try. CV algorithm chooses the best one. Can dictate number of knot by setting to one value. Set to zero to reduce to p-order polynomial.
%
% OUTPUTS
%       yhat - vector of predicted values at xx locations
%       par - parameter values (ctrl+f this function to see what they mean)
%
% This version also standardises data, and uses an alternative stable
% algorithm to optimise the smoothing parameters and knot locations, and to
% calculate predictive points and confidence bands.
%
% Note: can avoid some bugs with the Cholesky decomp by reducing the number
% of knots in the trial values.
%
% Tested and working WEB 13/01/2014
y=y(:);
x=x(:);
%% Settings
derivplot=0; % set to 1 to also give derivative plot
confcalc=1; % switch to turn off confidence intervals calculation

%% Set defaults
if nargin<3
    plott=0; % set default no plot
end
if nargin<5
    Kset=[5 10 20 40 80 120]; % trial values of numbers of knots - defaults to numbers suggested in Ruppert p.127
end
if nargin<6
    xx=linspace(min(x),max(x),50)'; % set default predictive points (50 evenly spaced)
end
if nargin<4
    p=3; % default cubic spline
end
par.p=p;
par.xx=xx;
par.x=x;
par.y=y;
%% standardise and sort data first
[x, order]=sort(x);
y=y(order);
meanx=mean(x);
stdx=std(x);
meany=mean(y);
x=(x-meanx)./stdx;
xx=(xx-meanx)./stdx;
y=y-meany;
xx=sort(xx); %ensure that xx is sorted to ensure sensible plot

%% Automatic knot and parameter selection
n=length(x);
nuniq=length(unique(x)); % find number of unique x, to be used with automatic knot selection, cf Ruppert p.127
if nuniq<2 % bit here to change the order of the spline when not enough data points to support the order.
    error('Only 1 unique x values, need at least 2 to fit any kind of model!')
elseif nuniq==2 && p>1
    warning('Only 2 unique x values found: reverting to linear model')
    p=1;
elseif nuniq==3 && p>2
    warning('Only 3 unique x values found: reverting to quadratic model')
    p=2;
elseif nuniq<5 && p>3
    warning('Not enough x values for high-order model: reverting to cubic model')
    p=3;
end
Kcut=nuniq-p-1; % for use with finding maximum K (no. knots) to consider. Is nuniq-p-1, where p is the order of the spline
xuniq=unique(x);

%Kset=[5 10 20 40 80 120]; % trial values of number of knots, used to optimise number of knots
par.Kset=Kset;
Kvals=Kset(Kset<=Kcut); %discard any K values that are greater than or equal to Kcut (Ruppert p.127). Otherwise cholesky decomp will not work.
if isempty(Kvals)
    Kvals=0;
    warning('You do not have enough unique x values to support your trial numbers of knots (Kset). Model has reverted to p-order polynomial. Please reduce values in Kset for spline fit.')
end
CV=zeros(length(Kvals),1);
lamd_vec=CV;

for i=1:length(Kvals)
    
    K=Kvals(i); % get trial K
    
    if K>0
        knot_qs=((1:K)+1)/(K+2);    % get knot locations
        kvec=quantilewill(xuniq,knot_qs)'; % consider quantiles of unique x values only - stops strange behaviour when lots of repeated x values are present
    else
        kvec=[];
    end
    
    [lamd_vec(i), CV(i)]=spline_will_CV(x,y,kvec,1,p); % find best lamd for these knot locations, and CV value at this best lamd
    %CV(i)=cubic_spline_lamfind_stable(x,y,kvec,lamd_vec(i)); % find CV measure of this
end
par.CV=CV;

[junk, i_best]=min(CV); % find minimum
nk=Kvals(i_best);           % best number of knots
par.nk=nk;
if nk>0
    knot_qs=((1:nk)+1)/(nk+2);    % get knot locations
    kvec=quantilewill(xuniq,knot_qs)'; % consider quantiles of unique x values only - stops strange behaviour when lots of repeated x values are present
else
    kvec=[];
end
par.kvec=kvec;
lamd=lamd_vec(i_best);
par.lamd=lamd;

%% Build basis matrices
if nk>0
    X_spline=repmat(x,1,nk)-repmat(kvec(:)',n,1); % building spline part of X i.e. ruppert p.61, standard X matrix in linear regression but with linear spline basis functions
    X_spline=max(X_spline,zeros(size(X_spline))); % make any entries zero if negative - this is now the correct spline part of X
    X_splinep=X_spline.^p; % cube to give cubed spline basis functions
    Xpol=[ones(n,1), repmat(x,1,p)];
    for ii=2:p+1
        Xpol(:,ii)=Xpol(:,ii).^(ii-1);
    end
    X=[Xpol X_splinep]; % now make full X matrix
else
    X=[ones(n,1), repmat(x,1,p)];
    for ii=2:p+1
        X(:,ii)=X(:,ii).^(ii-1);
    end
end

XXpol=[ones(length(xx),1), repmat(xx,1,p)];
for ii=2:p+1
    XXpol(:,ii)=XXpol(:,ii).^(ii-1);
end
if nk>0
    XX_spline=repmat(xx,1,nk)-repmat(kvec(:)',length(xx),1); % Now building predictive X matrix in same way
    XX_spline=max(XX_spline,zeros(size(XX_spline)));
    XX_splinep=XX_spline.^p;
    X_pred=[XXpol XX_splinep]; % now make full X matrix
else
    X_pred=[ones(length(xx),1), repmat(xx,1,p)];
    for ii=2:p+1
        X_pred(:,ii)=X_pred(:,ii).^(ii-1);
    end
end

D=zeros(nk+p+1);  % make D matrix from p.69 Ruppert
if nk>0
    D((p+2):end,(p+2):end)=eye(nk);
end
%% Stable calculation of yhat and confidence intervals - see appendix B1 pp.336-338
R=chol(X'*X + 10e-9*D); % get cholesky decomp of X'X such that R'R=X'X
[U, S]=svd(R'\D/R); % get singular value decomp of RDR thing. Note that U = V' (the third output of the svd, not called here)
s=diag(S);
A=X/R*U;
A_xx=X_pred/R*U; % this A is for use with predicting at new x points specified by xx.
b=A'*y;
%b_xx=A_xx'*y;
onesvec=ones(length(s),1); % this is a s-length vector of ones for use in following calcs

yhat=A*(b./(1+lamd^(2*p)*s)); % this is the alternative "stable" expression - see Ruppert p.336
yhat_xx=A_xx*(b./(1+lamd^(2*p)*s)); % prediction at xx points (equal to yhat if x=xx)

% Conf intervals part
if confcalc==1
    dfit=onesvec'*(1./(1+lamd^(2*p)*s));
    RSS=sum((y-yhat).^2);
    dfres= n - 2*dfit + sum((1./(1+lamd^(2*p)*s)).^2); %p.338
    sigsq=RSS/dfres; % get estimate of sigma-squared, variance of noise on points - Ruppert p.119
    sdevs=sqrt(sigsq)*sqrt(diag(A_xx*diag(1./(1+lamd^(2*p)*s))*A_xx')); %vector of standard deviations, as per p.337 (bottom)
    predbounds=sqrt(sigsq)*sqrt(1+diag(A_xx*diag(1./(1+lamd^(2*p)*s))*A_xx')); %vector of predictive sds.
else
    if plott==1
        plott=2; % turn off conf interval plotting since these quantities not calculated (see setting at beginning to turn on/off)
    end
end

%% Calculate directly coefficients and derivative
try
    beta=(X'*X+lamd^2*D)\X'*y;   % the vector of coefficients
    par.beta=beta;
    if nk>0
        Xderiv=[XXpol(:,(1:end-1)) XX_spline.^(p-1)];
        vec1=repmat([1:p p*ones(1,nk)],length(xx),1); % vector of coefficients due to differentiation
        Xderiv=Xderiv.*vec1;
    else % in the case where no knots and snaps to polynomial fit
        Xderiv=XXpol(:,(1:end-1));
        vec1=repmat(1:p,length(xx),1); % vector of coefficients due to differentiation
        Xderiv=Xderiv.*vec1;
    end
    fdash=Xderiv*beta(2:end);
catch
    err=lasterror;
    disp(err.message)
    disp('Error estimating parameters directly - skipped this part')
end
%% Adjust confidence intervals ready for plot
if exist('tinv','file')==2 && plott==1 % check whether tinv function is available - this is in the stats toolbox
    tfactor=tinv(0.975,dfres); % NOTE here I am using the assumption that n is small and yhat is distributed as a t-distribution with dfres DOFs (Ruppert p.140 eq.6.14)
    upper95=yhat_xx+tfactor*sdevs;
    lower95=yhat_xx-tfactor*sdevs;
    upper95_pred=yhat_xx+tfactor*predbounds;
    lower95_pred=yhat_xx-tfactor*predbounds;
else
    if plott==1
        disp('It looks like you do not have the stats toolbox. Confidence intervals cannot be plotted.')
        plott=2; % set switch to stop confidence band calculations as tfactor not available.
    end
end

%% un-standardise
x=x*stdx+meanx;
xx=xx*stdx+meanx;
y=y+meany;
yhat=yhat+meany;
yhat_xx=yhat_xx+meany;
if plott==1
    upper95=upper95+meany;
    lower95=lower95+meany;
    upper95_pred=upper95_pred+meany;
    lower95_pred=lower95_pred+meany;
end
fdash=fdash/stdx;
par.fdash=fdash;
par.yhat=yhat;
par.yhat_xx=yhat_xx;
par.res=y-yhat; % residuals

%% Plot
if plott==1
    f = [upper95; flipdim(lower95,1)];
    figure
    if derivplot==1
        subplot(2,1,1)
        fill([xx; flipdim(xx,1)], f, [7 7 7]/8, 'LineStyle','none')
        hold on
        plot(x,y,'.r')
        plot(xx,yhat_xx)
        plot(xx,upper95_pred,'g--')
        plot(xx,lower95_pred,'g--')
        xlabel('x')
        ylabel('y')
        legend('95% confidence intervals', 'Training data', 'Spline fit', '95% prediction intervals')
        title(['Penalized spline fit of order ' num2str(p) ' with ' num2str(nk) ' knots and smoothing parameter = ' num2str(lamd)])
        subplot(2,1,2)
        plot(xx,fdash)
        xlabel('x')
        ylabel('dy/dx')
        title('Derivative of y (NOT SCALED TO ORIGINAL DATA)')
    else
        fill([xx; flipdim(xx,1)], f, [7 7 7]/8, 'LineStyle','none')
        hold on
        plot(x,y,'.r')
        plot(xx,yhat_xx)
        plot(xx,upper95_pred,'g--')
        plot(xx,lower95_pred,'g--')
        xlabel('x')
        ylabel('y')
        legend('95% confidence intervals', 'Training data', 'Spline fit', '95% prediction intervals')
        title(['Penalized spline fit of order ' num2str(p) ' with ' num2str(nk) ' knots and smoothing parameter = ' num2str(lamd)])
    end
elseif plott==2 % plot without condidence intervals due to non-availability of tinv function (from stats toolbox), or on request
    %f = [upper95; flipdim(lower95,1)];
    figure
    if derivplot==1
        subplot(2,1,1)
        %fill([xx; flipdim(xx,1)], f, [7 7 7]/8, 'LineStyle','none')
        hold on
        plot(x,y,'.r')
        plot(xx,yhat_xx)
        %plot(xx,upper95_pred,'g--')
        %plot(xx,lower95_pred,'g--')
        xlabel('x')
        ylabel('y')
        legend('Training data', 'Spline fit')
        title(['Penalized spline fit of order ' num2str(p) ' with ' num2str(nk) ' knots and smoothing parameter = ' num2str(lamd)])
        subplot(2,1,2)
        plot(xx,fdash)
        xlabel('x')
        ylabel('dy/dx')
        title('Derivative of y (NOT SCALED TO ORIGINAL DATA)')
    else
        %fill([xx; flipdim(xx,1)], f, [7 7 7]/8, 'LineStyle','none')
        hold on
        plot(x,y,'.r')
        plot(xx,yhat_xx)
        %plot(xx,upper95_pred,'g--')
        %plot(xx,lower95_pred,'g--')
        xlabel('x')
        ylabel('y')
        legend('Training data', 'Spline fit')
        title(['Penalized spline fit of order ' num2str(p) ' with ' num2str(nk) ' knots and smoothing parameter = ' num2str(lamd)])
    end
end