function [lam, CV]=spline_will_CV(x,y,kvec,lam0,p)

% function to find the optimum smoothing parameter of kernel regression,
% using cross validation. Uses stable calculations in Appendix B of
% Ruppert.
%
% Works WEB 05/06/2012

n=length(x);
nk=length(kvec);

if nk>0
    X_spline=repmat(x,1,nk)-repmat(kvec(:)',n,1); % building spline part of X i.e. ruppert p.61, standard X matrix in linear regression but with linear spline basis functions
    X_spline=max(X_spline,zeros(size(X_spline))); % make any entries zero if negative - this is now the correct spline part of X
    X_spline=X_spline.^p; % cube to give cubed spline basis functions
    Xpol=[ones(n,1), repmat(x,1,p)];
    for ii=2:p+1
        Xpol(:,ii)=Xpol(:,ii).^(ii-1);
    end
    X=[Xpol X_spline]; % now make full X matrix
else
    X=[ones(n,1), repmat(x,1,p)];
    for ii=2:p+1
        X(:,ii)=X(:,ii).^(ii-1);
    end
end

D=zeros(nk+p+1);  % make D matrix from p.69 Ruppert
if nk>0
    D((p+2):end,(p+2):end)=eye(nk);
end

R=chol(X'*X + 10e-7*D); % get cholesky decomp of X'X such that R'R=X'X
[U, S]=svd(R'\D/R); % get singular value decomp of RDR thing. Note that U = V' (the third output of the svd, not called here)
s=diag(S);
A=X/R*U;
b=A'*y;

[lam, CV]=fminsearch(@(lam) spline_will_lamfind(A,b,s,y,n,p,lam),lam0);
