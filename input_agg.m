function y=input_agg(X,w,Aggr)

%Function that aggregates the 'X' matrix into a composite 'y', using the
%specified weights 'w'

%% Aggregate Composite Indicator
if strcmp(Aggr,'ArAv')==1
    Xw=X.*(w); 
    y=sum(Xw,2);
elseif strcmp(Aggr,'GeAv')==1
    Xw=X.^(w); 
    y=prod(Xw,2);
elseif strcmp(Aggr,'HarAv')==1
    Xw=w./X; 
    y=1./sum(Xw,2);
elseif strcmp(Aggr,'Cust')==1
% add customized aggregation function
end

end