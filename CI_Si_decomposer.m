function out=CI_Si_decomposer(X,y,plott,dep)

% A function which takes some X-y data and gives you first order
% sensitivity indices, plus each decomposed into correlated and
% uncorrelated contributions.
%
% Uses nonlinear regression of y on xi, and a nonlinear regression of xi on
% x-i according to the method of Xu and Gertner. Finally uses a NL
% regression of y on residuals of previous regression (in contrast to
% linear reg of X&G). Note that for NL dependence modelling, the stats
% toolbox is required. If this is not detected, it will use linear
% dependence modelling.
%
% Set plott=1 for plots, plott=0 for no plots
% Set dirname as string which is directory to save all results.

%% Some settings

if dep==1
lindep=1; % use linear dependence modelling.
elseif dep==2
lindep=0; % use nonlinear dependence modelling. NOTE: You need ML/stats toolbox to do NL dependence modelling, otherwise will go back to linear automatically.
end

% spline
p=3; % order of spline
knots=[1 2 3 4 5]; % vector of knot numbers to try (will select best number automatically)

[N,k]=size(X); % detect size of X
[r,c]=subplot_arranger(k); % get an optimized subplot-size for number of Xi
plotsize=[r,c]; % Set this subplot-size

%% Check toolboxes
verinfo=ver;
StatsTB=any(strcmp('Statistics and Machine Learning Toolbox', {verinfo.Name})); % flag for stats toolbox
if StatsTB==0 
    warning('It looks like you dont have the Stats/ML toolbox. Using linear dependence modelling instead of GPs.')
end

%% Get uncorrelated contribution: regress Xi on X-i (nonlinear reg.)

VU=zeros(k,1);
for ii=1:k % a loop here which regresses xi on x-i and gets residuals.
    Xloop=X;
    Xloop(:,ii)=[];
    if StatsTB==1&&lindep==0 % use GPs
        GPModel=fitrgp(Xloop,X(:,ii),'Sigma',std(y),'SigmaLowerBound',1e-7); % fit a low-noise GP for nonlinear regression of Xi on X-i.
        Residz=X(:,ii)-resubPredict(GPModel); % get residuals
    else
        Residz=X(:,ii)-[ones(N,1),Xloop]*([ones(N,1),Xloop]\X(:,ii)); % use linear dependence modelling (still multivariate, but linear)
    end
    yhat=spline_will(Residz,y); % NL regression y on resids
    VU(ii)=sum((yhat-mean(yhat)).^2)/(N-1); % get variance, i.e. Vi(xi_uncorrelated)
end

%% Linear estimates

if plott==1
    [R2,par_lin]=lin_SA(y,X,0);
else
    R2=lin_SA(y,X,0);
end

V=R2*var(y); % get partial variances
VC=V(:)-VU(:); % correlated part

out.lin.V=V; % save all to output
out.lin.VU=VU;
out.lin.VC=VC;
out.lin.S=R2;
out.lin.SU=VU/var(y);
out.lin.SC=VC/var(y);
out.lin.par=par_lin;


%% Spline estimates
if plott==1
    [S_E,junk,par_sp]=spline_SA(y,X,0,p,knots); % get spline estimates of Si

else
    S_E=spline_SA(y,X,0,p,knots);
end

V=S_E*var(y); % get partial variances
VC=V(:)-VU(:); % correlated part

out.spline.V=V; % save all to output
out.spline.VU=VU;
out.spline.VC=VC;
out.spline.S=S_E;
out.spline.SU=VU/var(y);
out.spline.SC=VC/var(y);
out.spline.par=par_sp;

%% Plot bar chart of results
if plott==1 % plot if requested
    %     figure
    %     bar([VU,VC]/var(y),'stacked') % stacked bar with SU and SC
    %     xlabel('Variable index')
    %     ylabel('Proportion of output variance')
    %     legend('Uncorrelated contribution', 'Correlated contribution')
    
    stackedbararray=zeros(2,k,2);
    stackedbararray(1,:,:)=[out.lin.SU,out.lin.SC];
    stackedbararray(2,:,:)=[out.spline.SU,out.spline.SC];
    stackedbararray=permute(stackedbararray,[2,1,3]); % have to rearrange array here otherwise plot is wrong
    bar1 = stackedgroupedbar(stackedbararray);
    xlabel('Variable index')
    ylabel('Proportion of output variance')
    set(get(get(bar1(2),'Annotation'),'LegendInformation'),'IconDisplayStyle','off'); % only disp one Si uncorr in legend
    legend('S_i (uncorrelated)','S_i (correlated,linear)','S_i (correlated, spline)')
    set(gcf,'units','normalized','outerposition',[0 0 1 1])
    set(bar1(1),'FaceColor',[0 0.447058826684952 0.74117648601532]); %blueish
    set(bar1(3),'FaceColor',[0 0.498039215803146 0]); %greenish
    set(bar1(2),'FaceColor',[0 0.447058826684952 0.74117648601532]); %blueish
    set(bar1(4),'FaceColor',[1 0.843137264251709 0]); %yellowish
end

%% Plot all smoothing fits
if plott==1
    k=size(X,2);
    if k <= plotsize(1)*plotsize(2)
        kplot=k;
    else
        kplot=plotsize(1)*plotsize(2);
    end
    for i=1:kplot
        figure(101)
        subplot(plotsize(1),plotsize(2),i)
        % plot data points
        plot(X(:,i),y,'.k')
        hold on
        % plot spline
        plot(out.spline.par.xsrts(:,i),out.spline.par.yhats(:,i),'r')
        % plot linear
        plot(out.lin.par.xsrts(:,i),out.lin.par.yhats(:,i),'c')
        
%         xlabel(['x_',num2str(i)]) %subscript only first digit
        xlabel(['x_{' num2str(i) '}']) % subscripts all digits 
        ylabel('y')
        legend('Data','Spl','Lin','Location','NorthWest')
        set(101,'units','normalized','outerposition',[0 0 1 1])
    end
end

%% Build results table

rownamez=[repmat('X_',k,1),num2str((1:k)','%-3i')];
rownamez=cellstr(rownamez); % cell array with vector names
varnames={'S_L','S_Sp','SU_L','SU_Sp','SC_L','SC_Sp'};
if verLessThan('matlab','8.2')==0 % only do this if matlab 2013b or later 
    out.results=array2table([out.lin.S,out.spline.S,out.lin.SU,out.spline.SU,out.lin.SC,out.spline.SC],'RowNames',rownamez,'VariableNames',varnames);
end
