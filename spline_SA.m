function [S_E,S_V,par]=spline_SA(y,X,plott,p,knots,outputname,inputnames)

% Function that calculates first-order sensitivity indices from "given"
% data. Makes a scatterplot of y against each input variable, then uses
% penalised spline regression to plot E(Y|X_i). Then, using the
% computational variance formula, finds the variance of this curve.
%
% INPUTS: y =    N-length column vector of outputs
%         X =    Nxk matrix of N input points in k-space
%         plott= Set to: 0  for no plot
%                        -1 for scatterplots of first six input variables
%                        i  to make a scatterplot of the ith variable
%         p = order of spline: 1=linear, 2=quadratic, etc.
%         knots= row vector of numbers of knots to try. Set to zero to
%                revert to simple polynomial
%         outputname = string of name of output variable (optional)
%
%         inputnames = cell array containing names of each input (optional, to be used in plots)
%
% OUTPUTS: S_E = k-length column vector of first-order sensitvity indices,
%                estimated via E(Y|Xi)
%          S_V = as S_E but estimated via V(Y|Xi)
%          par = a load of parameters and bits contained in a structure
%
% IMPORTANT NOTE: This code assumes that x points are distributed
% according to their underlying sample distributions. If this is not the
% case, this will not give accurate answer. To account for other
% distributions, e.g. uniform, one could get spline predictions for a
% uniform point set over the space of x. Would require a little tweaking.
%
% Last update ??

%%  Prep
varbias=0; % set this to 0 to use unbiased estimation of var(y) (i.e. normalised by n-1), or 1 to used biased estimation (normalised by n, makes it consistent with R^2 in the linear case)

y=y(:); % ensure y is column vector
[N,k]=size(X);
if k==1
    X=X(:); % ensure X is column if vector
end

if nargin<3
    plott=0;
end
if nargin<4
    p=3; % order of spline to fit to data
end
if nargin<5
    knots=[0:5:40]; % vector of numbers of knots to try in spline fit. Zero fits polynomial
end
if nargin<6
    outputname='y';
end
if nargin<7
    for ii=1:k
        inputnames(ii)={['x_',num2str(ii)]};
    end
end

if length(inputnames)~=k
    error('Number of input names not equal to number of inputs')
end

VEE=zeros(k,1);
EVE=zeros(k,1);
yhats=zeros(N,k);
varhats=zeros(N,k);
xsrts=zeros(N,k);
par.p=p;
par.trialknots=knots;
nplot=50; % number of equally-spaced points to use in plots
nderiv=11; % number of equally-spaced points to use in derivative estimates
knotN=zeros(k,1); % vector to record how many knots in each spline fit
derivs=zeros(nderiv,k);
derivs_xx=zeros(nderiv,k);

%% Calc
if plott==0 % no plot
    for i=1:k
        [xst, order]=sort(X(:,i));
        xsrts(:,i)=xst(:);
        yst=y(order);
        if length(unique(xst))<2
            VEE(i)=0;
            disp(['Warning: input variable number ',num2str(i),' cannot be regressed because it has only 1 unique x value. Si set to zero.'])
        else
            [yhat, par2]=spline_will(xst,yst,0,p,knots,xst);  % get spline fit
            yhats(:,i)=yhat(:); % record yhat in matrix
            VEE(i)=mean(yhat.^2)-mean(yhat)^2;   % calculate var(E(Y|X_i))
            knotN(i)=par2.nk; % record how many knots are used
            deriv_xx=linspace(min(xst),max(xst),nderiv)'; % vector of x points to use for calculation of derivatives (specially inserted for CI work)
            derivs_xx(:,i)=deriv_xx;
            [junk, par3]=spline_will(xst,yst,0,p,knots,deriv_xx);  % get spline fit at deriv points
            derivs(:,i)=par3.fdash; % record in matrix
            sqres=(par2.res).^2; % get residuals
            varhat=spline_will(xst,sqres,0,p,knots,xst); % regress squared residuals on x
            varhats(:,i)=varhat(:); % record varhat in matrix
            EVE(i)=mean(varhat);
        end
    end
elseif plott==-1    % first six variables plotted
    figure
    for i=1:k
        [xst, order]=sort(X(:,i));
        xsrts(:,i)=xst(:);
        yst=y(order);
        if length(unique(xst))<2
            VEE(i)=0;
            disp(['Warning: input variable number ',num2str(i),' cannot be regressed because it has only 1 unique x value. Si set to zero.'])
        else
            [yhat, par2]=spline_will(xst,yst,0,p,knots,linspace(min(xst),max(xst),nplot)');  % get spline fit
            yhats(:,i)=yhat(:); % record yhat in matrix 
            VEE(i)=mean(yhat.^2)-mean(yhat)^2;   % calculate var(E(Y|X_i))
            knotN(i)=par2.nk; % record how many knots are used
            deriv_xx=linspace(min(xst),max(xst),nderiv)'; % vector of x points to use for calculation of derivatives (specially inserted for CI work)
            derivs_xx(:,i)=deriv_xx;
            [junk, par3]=spline_will(xst,yst,0,p,knots,deriv_xx);  % get spline fit at deriv points
            derivs(:,i)=par3.fdash; % record in matrix
            sqres=(par2.res).^2; % get residuals
            varhat=spline_will(xst,sqres,0,p,knots,xst); % regress squared residuals on x
            varhats(:,i)=varhat(:); % record varhat in matrix
            EVE(i)=mean(varhat);
            if i<7
                S=VEE(i)/var(y);
                Sround=round(S*100)/100;
                subplot(2,3,i)
                plot(X(:,i),y,'.')
                hold on
                plot(linspace(min(xst),max(xst),nplot),par2.yhat_xx,'g')
                title(['S_i = ' num2str(Sround),' knots = ',num2str(par2.nk)]);
                legend('Data','Spline','Location','NorthWest')
                xlabel(char(inputnames(i)))
                ylabel(outputname)
            end
        end
    end
else %plot ith variable
    for i=1:k
        [xst, order]=sort(X(:,i));
        xsrts(:,i)=xst(:);
        yst=y(order);
        if length(unique(xst))<2
            VEE(i)=0;
            disp(['Warning: input variable number ',num2str(i),' cannot be regressed because it has only 1 unique x value. Si set to zero.'])
        else
            [yhat, par2]=spline_will(xst,yst,0,p,knots,linspace(min(xst),max(xst),nplot)'); % get spline fit
            yhats(:,i)=yhat(:); % record yhat in matrix
            VEE(i)=mean(yhat.^2)-mean(yhat)^2;   % calculate var(E(Y|X_i))
            knotN(i)=par2.nk; % record how many knots are used
            deriv_xx=linspace(min(xst),max(xst),nderiv)'; % vector of x points to use for calculation of derivatives (specially inserted for CI work)
            derivs_xx(:,i)=deriv_xx;
            [junk, par3]=spline_will(xst,yst,0,p,knots,deriv_xx);  % get spline fit at deriv points
            derivs(:,i)=par3.fdash; % record in matrix
            sqres=(par2.res).^2; % get residuals
            varhat=spline_will(xst,sqres,0,p,knots,xst); % regress squared residuals on x
            varhats(:,i)=varhat(:); % record varhat in matrix
            EVE(i)=mean(varhat);
            if i==plott
                S=VEE(i)/var(y);
                Sround=round(S*100)/100;
                figure
                plot(X(:,i),y,'.')
                hold on
                plot(linspace(min(xst),max(xst),nplot),par2.yhat_xx,'g')
                legend('Data','Spline','Location','NorthWest')
                title(['S_i = ' num2str(Sround),' knots = ',num2str(par2.nk)]);
                xlabel(char(inputnames(i)))
                ylabel(outputname)
            end
        end
    end
end

par.knotN=knotN; % number of knots used for each variable
par.derivs=derivs; % derivative values
par.derivs_xx=derivs_xx; % x values for derviative values
par.yhats=yhats; % main effect lines for each variable
par.varhats=varhats; % conditional variance lines for each variable
par.xsrts=xsrts; % sorted x-values for each variable - for use plotting scatterplots

if varbias==1
    S_E=VEE/var(y,1); %standardise by total variance NOTE USING THE BIASED ESTIMATOR OF VARIANCE I.E. NORMALISE BY N INSTEAD OF N-1 (this makes it consistent with R^2).
    S_V=1-EVE/var(y,1);
else
    S_E=VEE/var(y); %standardise by total variance (unbiased estimator of var(y))
    S_V=1-EVE/var(y);
end
% if plott~=0
%  figure
%  bar(S)
%  xlabel('Variable index')
%  ylabel('S_i estimate')
% end