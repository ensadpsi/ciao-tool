function CVlam=spline_will_lamfind(A,b,s,y,n,p,lamd)

% a function that computes the generalised CV measure for a given lambda value and
% given x-y data, using a cubic spline. See GCV on p.117 and stable
% calculation on p. 337 (Ruppert).
%
% Note this is a more stable version built according to the principles in
% appendix B, Ruppert, p. 336.

yhat=A*(b./(1+lamd^(2*p)*s)); % this is the alternative "stable" expression - see Ruppert p.336
dfit=ones(1,length(s))*(1./(1+lamd^(2*p)*s)); % lam^(p*2) because we are using a cubic smoother - see bottom of p69 Ruppert for reasoning.
RSS=sqrt(sum((y-yhat).^2));

%Slam=X_pred/(X'*X+lamd^2*D)*X';   % the smoothing matrix
%yhat=Slam*y; % get y hat (ruppert p.66)

%CVlam=sum(((y-yhat)./(1-dfit)).^2);
CVlam=RSS/(1-dfit/n)^2; % % get generalised CV measure.