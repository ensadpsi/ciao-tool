clear
clc
close all

% This is a menu version for the assessment of implicit weights in
% composite indicators: the Composite Indicator Analysis and Optimization
% (CIAO) tool. For a detailed overvie of all functions in this package,
% see the Matlab guide (CIAO_Matlab_guide.pdf)


% First, a matrix named 'X', containing all the indicator data, needs to be imported and saved
% For this version, a demo dataset is automatically loaded (test_data.mat).
% 


%% Load input data

load test_data   % load X matrix containing indicator data

%% Name variables

[n,k]=size(X);
rownamez=[repmat('X_',k,1),num2str((1:k)','%-3i')]; % change 'X_' to desired name
rownamez=cellstr(rownamez); % cell array with vector names
varnamez=rownamez';

%% Check correlation and P-values

[R,P] = corrcoef(X);
fprintf('Correlation matrix >>\n')
corr=array2table(round(R,4),'RowNames',rownamez, 'VariableNames', varnamez);
disp(corr)
fprintf('P-values >>\n')
pval=array2table(round(P,4),'RowNames',rownamez, 'VariableNames', varnamez);
disp(pval)

%% Set fontsize for Menu GUI

UIControl_FontSize_bak = get(0, 'DefaultUIControlFontSize');
set(0, 'DefaultUIControlFontSize', 20);

%% Choose aggregation function and weights

w_scenario=menu('Assign weights','Equal','Custom');
if w_scenario==1
    w=ones(1,k).*(1/k); %creates a column-vector with equal weights
elseif w_scenario==2
    if k>20     % check if number of indicators is too large for custom weights
       fprintf('Too many indicators for custom weights, equal weights used >>\n')
       w=ones(1,k).*(1/k); %creates a column-vector with equal weights
    else
       w=input_weights(X); % function that calls an inputdlg-window for input of wieghts.
    end
end

fprintf('Weights selected >>\n')
disp(w') % remove ' for horizonatal disp

% aggregation=menu('Select aggregation method','ArAv','GeAv','HarAv','Cust (needs manual fix)');
aggregation=menu('Select aggregation method','ArAv','GeAv','HarAv');
if aggregation==1
    fprintf('Arithmetic average aggregation selected.\n') % no need to replace zeros
    y=input_agg(X,w,'ArAv'); % aggregated CI
    
elseif aggregation==2
    fprintf('Geometric average aggregation selected.\n')
    null_val=ismember(0,X); % check matrix for zero values
    if null_val==1
        max_X=max(max(X)); % Get maximum value in X
        X=(X*0.99)+(max_X*0.01);  % need to replace zero values for GeAv
    end
    y=input_agg(X,w,'GeAv'); % aggregated CI
    
elseif aggregation==3
    fprintf('Harmonic average aggregation selected.\n')
    null_val=ismember(0,X); % check matrix for zero values
    if null_val==1
        max_X=max(max(X)); % Get maximum value in X
        X=(X*0.99)+(max_X*0.01);  % need to replace zero values for HarAv
    end
    y=input_agg(X,w,'HarAv'); % aggregated CI
    
elseif aggregation==4
    %     fprintf('Custom aggregation.\n')
    %     Add a custom function in:
    %     y=input_agg(X,w,'Cust'); % aggregated CI
end

%% Estimation and Decomposition of influence

decomp_choice=menu('Select dependence modelling for decomposition','Linear','Nonlinear');
if decomp_choice==1
    outSi=CI_Si_decomposer(X,y,1,decomp_choice);
    fprintf('Si (lin and nonlin) and decomposed parts (linear) >>\n')
    disp(outSi.results) % display results-table with all results
elseif decomp_choice==2
    outSi=CI_Si_decomposer(X,y,1,decomp_choice);
    fprintf('Si (lin and nonlin) and decomposed parts (nonlinear) >>\n')
    disp(outSi.results) % display results-table with all results
end

%% Optimization

objective=menu('Select weight optimization objective','Adjust','Maximize');
if objective==1
    Sd=w; % Target of the "Adjust" optimization is the weight values
elseif objective==2
    Sd=ones(k,1); % Target values for the "Maximize" optimization
end
w_choice=menu('Select weight constraint for optimization','Sum to one','Sum to one AND be positive');
if aggregation==1
    outOpt=CIoptimiser(X,Sd,objective,w_choice,'ArAv');
elseif aggregation==2
    outOpt=CIoptimiser(X,Sd,objective,w_choice,'GeAv');
elseif aggregation==3
    outOpt=CIoptimiser(X,Sd,objective,w_choice,'HarAv');
elseif aggregation==4
%    outOpt=CIoptimiser(X,Sd,objective,w_choice,'Cust'); 
end

fprintf('Optimised weights >>\n') % you should now see that the optimised weights are:
disp(outOpt.wopt') % optimised weights

fprintf('Si at optimised weights >>\n') % and the corresponding sensitivity indices at these weights are
disp(outOpt.Sopt) % optimised Si

%% Table of results (Only if "Adjust" objective is chosen)
if objective==1
%First, Initial Si
fprintf('Normalised Si at initial weights >>\n')
Sis_norm=round((outSi.spline.S)./sum(outSi.spline.S), 5); % normalize Si at input weights, round to 5 decimals

% Deviation from target
Sis_target=w';   % target value is the input weights
Sis_dev=(Sis_norm-Sis_target); % discrepancy from target
Sis_ratio=round(Sis_norm./Sis_target, 2)-1; % make it into a ratio, round to 2 decimals
Dev_ratio= cellstr(num2str(100*Sis_ratio(:,1), '%g%%')); %add percentage symbols

T=table(Sis_norm,Sis_target,Sis_dev,Dev_ratio,'RowNames',rownamez); % build a table
disp(T)

% Second, Optimized Si
fprintf('Normalised Si at optimised weights >>\n')
Sisopt=outOpt.Sopt;
Sisopt_norm=round(Sisopt./sum(Sisopt), 4); % normalize Si at optimal weights, round to 4 decimals

% Deviation from desired influence
Sisopt_dev=(Sisopt_norm-Sis_target); % discrepancy from target
Sisopt_ratio=round(Sisopt_norm./Sis_target, 2)-1; % make it into a ratio, round to 2 decimals
Dev_ratio= cellstr(num2str(100*Sisopt_ratio(:,1), '%g%%')); %add percentage symbols

Topt=table(Sisopt_norm,Sis_target,Sisopt_dev,Dev_ratio,'RowNames',rownamez); % build a table of deviation
disp(Topt)
end

%% Fontsize of GUIs

set(0, 'DefaultUIControlFontSize', UIControl_FontSize_bak); % sets for all Menu GUI in this code

%% The end
% Enjoy


