function out=randmatrix(X)
% This function creates a random matrix with starting points for the
% multistart-solver. Values for each startingpoint are determined by the
% size of X and with the weigth constraints. Furthermore, this function
% offers the possibility to choose how many startpoints you wish to
% generate for the multistart. 

[~,k]=size(X);

A= {'Choose how many starting points for Multistart'};
w_choice=inputdlg(A,'Number of starting points',[1 100]);
w=str2num(w_choice{1});

for i=1:w
    currentdir_save=cd; % save current working directory
    cd('randfixedsum') % got to subfolder
    strt_pts(:,i)=randfixedsum(k,1,1,0,1);
    cd(currentdir_save) % go back to original directory
end

ex_w=strt_pts;

out=ex_w'; % shift to horizontal to match the dimension of the start point for the optimization