function w=input_weights(X)

[n,k]=size(X);

%% Ask for input weights
A={['Assign weights to the ' num2str(k) ' indicators (only values between 0 and 1, and weights must sum to one). Separate each value by space. ' ]};
B=ones(1,k).*(1/k); %creates a column-vector with equal weights, round to 4
w_choice=inputdlg(A,'Weights',[1 250],{num2str(B)}); % input weight window
w=str2num(w_choice{1}); %convert input-weights to numeric values

w_round=round(sum(w),4); % need to round before loop, due to floating point operations.
tol = eps(0.5); % Compare floating-point numbers using a tolerance

%% Error message if weight input exceedes constraints
while length(w)<k || length(w)>k || abs(w_round-1)>tol
    A={['Assign weights to the ' num2str(k) ' indicators CORRECTLY (only values between 0 and 1, and weights must sum to one). Separate each value by space. ']};
    w_choice=inputdlg(A,'Weights',[1 250],{num2str(B)});
    w=str2num(w_choice{1});
end

