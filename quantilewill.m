function q = quantilewill(x,p,plott)

% this is a replacement for the quantile function in the stats toolbox.
% Only does vectors though, not matrices.
%
% x is a vector of values
% p is a vector of quantile values
% q returns the values of the p-quantiles of x.
%
% set plott=1 for a graphical plot of the interpolated cdf.
%
% Uses linear interpolation. See Matlab help on Quantiles and Percentiles
% which shows how this code works.

if nargin<3
    plott=0;
end

x=x(:);

xsrt=sort(x);
n=length(x);
qx=[0, (0.5:n-0.5)/n, 1]'; % assign quantile values to midpoints between sorted x values
xx=[xsrt(1); xsrt; xsrt(end)]; % make xx vector which is sorted x plus extra x value on each end (see matlab help to see why)

q = interp1(qx,xx,p(:)); % interpolate cdf to find new quantiles

if plott==1 % optional plot of cdf
    figure
    plot(qx,xx)
    hold on
    hh=stem(p,q,'r');
    set(hh,'BaseValue',min(xx));
end
