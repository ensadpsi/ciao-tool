This is a small collection of functions designed for analysing weights in composite indicators in Matlab.

To see what they can do, run MENU_version, which uses all the functions here.

The main functions are:

 - CI_Si_decomposer: using CI X and y data, gets estimates of Si, both correlated and uncorrelated and gives scatter plots with splines and linear regression fits. [REQUIRES ML/STATS TOOLBOX for NL estimation of dependence, otherwise defaults back to linear]

 - CIoptimiser: given X data, finds weights that correspond to "target importance" of each indicator [REQUIRES OPTIMISATION TOOLBOX for constrained optimisation, otherwise defaults to unconstrained]

 - lin_SA: gives linear estimates of Si for X and y data

 - spline_SA: gives nonlinear (spline) estimates of Si for X and y data

 - spline_will: fits penalised smoothing splines to data

Other functions here are auxilliary functions to support those listed above.

Please see the following reference for more details:

Becker, W., Saisana, M., Paruolo, P., Vandecasteele, I., Weights and Importance in Composite Indicators: Closing the Gap (submitted to Ecological Indicators)

**** UPDATE 29/05/2017 ****

Scripts will now run without additional toolboxes. However, without the ML/stats toolbox the dependence modelling will be linear (estimation of correlation ratio will still be nonlinear). Without the optimisation toolbox, the optimisation of weights will be unconstrained.

If you have an older version of the optimisation toolbox, you might encounter an error, that "optimoptions" is not a recognised function. If this occurs, set usebasicopt=1 in the optimisation function.

Any questions william.becker@ec.europa.eu