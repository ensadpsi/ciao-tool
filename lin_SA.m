function [R2,par]=lin_SA(y,X,plott,outputname,inputnames)

% Calculates R^2 values just by doing a 1D linear regression on each variable
% separately. The idea is to compare these with Si values from the kernel
% or spline script.

%%  Prep
y=y(:); % ensure y is column vector
[N,k]=size(X);
if k==1
    X=X(:); % ensure X is column if vector
end

if nargin<3
    plott=0;
end
if nargin<4
    outputname='y';
end
if nargin<5
    for ii=1:k
        inputnames(ii)={['x_',num2str(ii)]};
    end
end

if length(inputnames)~=k
    error('Number of input names not equal to number of inputs')
end

R2=zeros(k,1);
yhats=zeros(N,k);
xsrts=zeros(N,k);
%% Calc
if plott==0 % no plot
    for i=1:k
        [xst, order]=sort(X(:,i));
        xsrts(:,i)=xst(:);
        yst=y(order);
        yhat=[ones(N,1),xst]*([ones(N,1),xst]\yst); % get linear model fit
        yhats(:,i)=yhat;
        R2(i)=1-sum((yst - yhat).^2)/sum((yst - mean(yst)).^2); % get R2
    end
elseif plott==-1    % first six variables plotted
    figure
    for i=1:k
        [xst, order]=sort(X(:,i));
        xsrts(:,i)=xst(:);
        yst=y(order);
        yhat=[ones(N,1),xst]*([ones(N,1),xst]\yst); % get linear model fit
        yhats(:,i)=yhat;
        R2(i)=1-sum((yst - yhat).^2)/sum((yst - mean(yst)).^2); % get R2
        if i<7
            R2round=round(R2(i)*100)/100;
            subplot(2,3,i)
            plot(X(:,i),y,'.')
            hold on
            plot(xst,yhat,'g')
            title(['R^2 = ' num2str(R2round)]);
            legend('Data','LR','Location','NorthWest')
            xlabel(char(inputnames(i)))
            ylabel(outputname)
        end
    end
else %plot ith variable
    for i=1:k
        [xst, order]=sort(X(:,i));
        xsrts(:,i)=xst(:);
        yst=y(order);
        yhat=[ones(N,1),xst]*([ones(N,1),xst]\yst); % get linear model fit
        yhats(:,i)=yhat;
        R2(i)=1-sum((yst - yhat).^2)/sum((yst - mean(yst)).^2); % get R2
        if i==plott
            R2round=round(R2(i)*100)/100;
            figure
            plot(X(:,i),y,'.')
            hold on
            plot(xst,yhat,'g')
            title(['R^2 = ' num2str(R2round)]);
            legend('Data','LR','Location','NorthWest')
            xlabel(char(inputnames(i)))
            ylabel(outputname)
        end
    end
end

par.yhats=yhats;
par.xsrts=xsrts;